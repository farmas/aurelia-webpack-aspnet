﻿import {Aurelia} from 'aurelia-framework';

// we want font-awesome to load as soon as possible to show the fa-spinner
import '../css/main.css';
import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap';

// comment out if you don't want a Promise polyfill (remove also from webpack.config.js)
import * as Bluebird from 'bluebird';
Bluebird.config({ warnings: false });

import 'isomorphic-fetch';

export async function configure(aurelia: Aurelia) {
    aurelia.use
        .standardConfiguration()
        .developmentLogging();

    await aurelia.start();
    aurelia.setRoot('app');
}