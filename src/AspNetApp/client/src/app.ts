import {HttpClient, json} from 'aurelia-fetch-client';
import {autoinject} from 'aurelia-framework';

class Todo {
    public isComplete = false;
    public id: string;

    constructor(public text: string) {
    }
}

@autoinject
export class App {
    private _http: HttpClient;

    public todos: Todo[] = [];
    public newTodoText = '';

    constructor(http: HttpClient) {
        this._http = http;
    }

    public async activate() {
        let response = await this._http.fetch('api/todos');
        this.todos = await response.json();
    }

    public async addTodo() {
        if (this.newTodoText) {
            let todo = new Todo(this.newTodoText);
            this.todos.push(todo);
            this.newTodoText = '';

            let response = await this._http.fetch('api/todos', {
                method: 'POST',
                body: json(todo)
            });

            let serverTodo: Todo = await response.json();
            todo.id = serverTodo.id;
        }
    }

    public removeTodo(todo: Todo) {
        let index = this.todos.indexOf(todo);
        if (index !== -1) {
            this.todos.splice(index, 1);
        }

        this._http.fetch('api/todos/' + todo.id, {
            method: 'DELETE'
        });
    }

    public setIsComplete(todo: Todo) {
        todo.isComplete = !todo.isComplete;
        this._http.fetch('api/todos/' + todo.id, {
            method: 'PUT',
            body: json(todo)
        });
    }
}