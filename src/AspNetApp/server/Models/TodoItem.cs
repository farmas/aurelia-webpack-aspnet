﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetApp.Models
{
    public class TodoItem
    {
        public string Id { get; set; }
        public string Text { get; set; }
        public bool IsComplete { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
