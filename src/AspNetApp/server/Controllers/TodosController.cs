﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspNetApp.Models;
using System.Collections.Concurrent;

namespace AspNetApp.Controllers
{
    [Route("api/[controller]")]
    public class TodosController : Controller
    {
        private static ConcurrentDictionary<string, TodoItem> _todos = new ConcurrentDictionary<string, TodoItem>();

        static TodosController()
        {
            _todos.TryAdd("1", new TodoItem() { Id = "1", Text = "My first todo", CreatedDate = DateTime.Now });
            _todos.TryAdd("2", new TodoItem() { Id = "2", Text = "My second todo", CreatedDate = DateTime.Now });
        }

        [HttpGet]
        public IEnumerable<TodoItem> Get()
        {
            var todos = _todos.Values.ToList();
            todos.Sort((x, y) => x.CreatedDate.CompareTo(y.CreatedDate));
            return todos;
        }

        [HttpPost]
        public TodoItem Post([FromBody]TodoItem todoItem)
        {
            todoItem.Id = Guid.NewGuid().ToString();
            todoItem.CreatedDate = DateTime.Now;
            _todos.TryAdd(todoItem.Id, todoItem);
            return todoItem;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody]TodoItem todoItem)
        {
            TodoItem serverTodo;
            if (_todos.TryGetValue(id, out serverTodo))
            {
                serverTodo.IsComplete = todoItem.IsComplete;
                serverTodo.Text = todoItem.Text;

                _todos.TryUpdate(id, serverTodo, serverTodo);
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            TodoItem todo;
            _todos.TryRemove(id, out todo);
        }
    }
}
