# Aurelia + WebPack + ASP.NET Core #

This is a skeleton project based on [Aurelia's Skeleton Navigation](https://github.com/aurelia/skeleton-navigation) that configures Aurelia to use WebPack and ASP.NET Core back end. It simplifies the dependencies from the Skeleton Navigation to the minimum and is ready to be deployed to an Azure WebApp.

## Requirements ##

* Node >= 4
* NPM >= 3
* .NET Core 1.1
* VisualStudio 15 with Update 3

## Setup Development Environment ##

* Client side
    * Change directory to 'src/AspNetApp'
    * Run 'npm install'
    * Run 'npm run server'
    * This will start the webpack-dev-server and serve the app on localhost:9000
* Server side (with VisualStudio)
    * Open solution from repo root.
    * Restore all packages.
    * Press 'F5' to run server.
* Server side (without VisualStudio)
    * Change directory to 'src/AspNetApp'
    * Run 'dotnet restore'
    * Run 'dotnet run'

Once the ASP.NET server is running it can be reached on localhost:5000, and the WebApi is accessible on localhost:5000/api/todos. Note: the webpack-dev-server is already configured to proxy call to 'api/todos' to the ASP.NET server.

## Build Production Bundle ##

* Change directory to 'src/AspNetApp'
* Run 'npm run build:prod'
* This will create a bundle on 'wwwroot' that ASP.NET is already configured to serve as static files.

## Publish on Azure ##

* Create a WebApp on Azure
* Make sure that it is configured with Node >= 4 and NPM >= 3.
* Push the repo to the WebApp.
* Upon deployment, Azure will create a webpack production bundle before pushing the website.